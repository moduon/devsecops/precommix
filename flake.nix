{
  description = "pre-commit mix with nix";

  nixConfig = {
    # HACK https://github.com/NixOS/nix/issues/6771
    # TODO Leave only own cache settings when fixed
    extra-trusted-public-keys = [
      "copier.cachix.org-1:sVkdQyyNXrgc53qXPCH9zuS91zpt5eBYcg7JQSmTBG4="
      "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw="
      "moduon.cachix.org-1:sXMrTN5LuhZyh6CnzYXM4pZkah/Yy//eGKt1oOZc0zw="
      "numtide.cachix.org-1:2ps1kLBUWjxIneOy1Ik6cQjb41X0iXVXeHigGmycPPE="
    ];
    extra-substituters = [
      "https://copier.cachix.org"
      "https://devenv.cachix.org"
      "https://moduon.cachix.org"
      "https://numtide.cachix.org"
    ];
  };

  inputs = {
    devenv.url = "github:cachix/devenv";
    devshell.url = "github:numtide/devshell";
    flake-compat.url = "github:edolstra/flake-compat";
    flake-parts.url = "github:hercules-ci/flake-parts";
    mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";
    nix2container.url = "github:nlewo/nix2container";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    dream2nix.url = "github:nix-community/dream2nix";
    dream2nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs:
    inputs.flake-parts.lib.mkFlake {inherit inputs;} ({
      lib,
      self,
      ...
    }: rec {
      imports = [
        flake.flakeModules.default
        flake.flakeModules.devshell
        flake.flakeModules.devenv
        ./checks.nix
      ];

      # Our own default dev shell is backed by devenv, but we expoxe a separate
      # one named "devshell" and backed by devshell to maintain compatibility.
      # TODO Deprecate devshell in favor of devenv
      precommix.devshellName = "devshell";

      flake = {
        # Probably useless overlay; keeping for backwards compatibility
        overlays.default = final: prev: {
          precommix-env = self.packages.${prev.system}.default;
        };

        flakeModules = {
          systems = {
            systems = lib.mkDefault lib.systems.flakeExposed;
          };

          # This module adds default system and adds precommix to flake inputs
          default = {
            _module.args.precommix = self;
            imports = [flake.flakeModules.systems];
          };

          # This module provides a devshell with precommix
          devshell = {config, ...}: {
            imports = [
              flake.flakeModules.default
              inputs.devshell.flakeModule
            ];

            options.precommix.devshellName = lib.mkOption {
              description = "The name of the devshell to create";
              default = "default";
              type = lib.types.str;
            };

            config.perSystem.devshells.${config.precommix.devshellName} = flake.devshellModules.default;
          };

          # Provide a default shell powered by devenv
          devenv = {config, ...}: {
            imports = [
              flake.flakeModules.default
              inputs.devenv.flakeModule
            ];

            options.precommix.devenvName = lib.mkOption {
              description = "The name of the devenv shell to create";
              default = "default";
              type = lib.types.str;
            };

            config.perSystem.devenv.shells.${config.precommix.devenvName}.imports = [
              flake.devenvModules.default
            ];
          };
        };

        # This module integrates precommix with devenv
        devenvModules.default = {pkgs, ...}: {
          env.SSL_CERT_FILE = "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt";

          # TODO Use https://devenv.sh/reference/options/#pre-commithooks instead
          packages = [
            (pkgs.precommix-env or self.packages.${pkgs.system}.default)
          ];

          enterShell = ''
            pre-commit install -f -t pre-commit -t commit-msg

            # HACK https://github.com/numtide/devshell/issues/172#issuecomment-1094675420
            export CPPFLAGS="-I$DEVENV_ROOT/include"
            export LDFLAGS="-L$DEVENV_ROOT/lib"
            export LD_LIBRARY_PATH="$DEVENV_ROOT/lib:$LD_LIBRARY_PATH"
          '';
        };

        # Include this devshell module in your flake to enable precommix
        # TODO Deprecate devshell module in favor of devenv module
        devshellModules = {
          default = {pkgs, ...}: let
            # Get overlaid precommix-env if found; otherwise, use the package
            # directly provided by our own flake
            precommix-env = pkgs.precommix-env or self.packages.${pkgs.system}.default;
          in {
            imports = [
              "${inputs.devshell}/extra/locale.nix"
            ];
            commands = [
              # List only apps that are actually useful on an interactive devshell
              {
                category = "precommix";
                help = precommix-env.config.deps.commitizen.meta.description;
                name = "cz";
                package = precommix-env;
              }
              {
                category = "precommix";
                help = precommix-env.config.deps.pre-commit.meta.description;
                name = "pre-commit";
                package = precommix-env;
              }
              {
                category = "precommix";
                help = precommix-env.config.deps.opentofu.meta.description;
                name = "tofu";
                package = precommix-env;
              }
            ];
            devshell.startup.precommix.text = ''
              pre-commit install -f -t pre-commit -t commit-msg
            '';
            devshell.packages = [
              pkgs.cacert
            ];

            env = [
              {
                name = "SSL_CERT_FILE";
                eval = "$DEVSHELL_DIR/etc/ssl/certs/ca-bundle.crt";
              }
            ];
          };
          buildVars = {
            env = [
              # HACK https://github.com/numtide/devshell/issues/172#issuecomment-1094675420
              {
                name = "CPPFLAGS";
                eval = "-I$DEVSHELL_DIR/include";
              }
              {
                name = "LDFLAGS";
                eval = "-L$DEVSHELL_DIR/lib";
              }
              {
                name = "LD_LIBRARY_PATH";
                eval = "$DEVSHELL_DIR/lib:$LD_LIBRARY_PATH";
              }
            ];
          };
        };
      };
      perSystem = {
        config,
        inputs',
        pkgs,
        self',
        system,
        ...
      }: let
        splitSystem = builtins.split "-" pkgs.system;
      in rec {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          overlays = [
            inputs.devshell.overlays.default
          ];
        };

        legacyPackages = inputs.dream2nix.lib.importPackages {
          projectRoot = ./.;
          projectRootFile = "flake.nix";
          packagesDir = "dream2nix/packages";
          packageSets.nixpkgs = pkgs;
          packageSets.own = legacyPackages;
        };

        packages.default = legacyPackages.precommix-env;
        packages.image = inputs'.nix2container.packages.nix2container.buildImage {
          name = "registry.gitlab.com/moduon/precommix";
          tag = "latest";

          config = {
            author = "Moduon";

            architecture = builtins.elemAt splitSystem 0;
            os = builtins.elemAt splitSystem 2;

            Cmd = ["bash"];

            Env = [
              "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
            ];
          };

          copyToRoot = packages.default;

          layers = [
            # Packages required to run CI workloads
            (inputs'.nix2container.packages.nix2container.buildLayer rec {
              maxLayers = builtins.length copyToRoot;
              copyToRoot = with pkgs; [
                bashInteractive
                cacert
                coreutils
                git
              ];
            })
          ];
        };

        devshells.ci = {
          imports = [flake.devshellModules.default];
          devshell.packages = [
            packages.default
            pkgs.git
          ];
        };

        checks =
          packages
          // lib.mapAttrs' (name: value: {
            name = "devShell-${name}";
            inherit value;
          })
          self.devShells.${system};
      };
    });
}
