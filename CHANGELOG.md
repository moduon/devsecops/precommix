## v0.21.0 (2024-05-28)

### Feat

-   provide and default to using a flake-parts module

### Fix

-   **direnv**: provide default flake location

### Refactor

-   **nix**: allow using without overlay, and default to it

## v0.20.0 (2024-05-02)

### Fix

-   **ctt**: move hook to the top

### Refactor

-   **devshellModules**: split out build vars fix in new module
-   **cz**: use pyproject.toml instead, if available
-   use ruff instead of other tools

## v0.19.0 (2024-04-12)

### Feat

-   **ci**: enable `parse-toml-timestamps` experimental feature

### Fix

-   **ansible-lint**: don't force color
-   **tofu**: advertise it in welcome message from devshell
-   **poetry**: remove from provided env, as its support was dropped in v0.18.0
-   **imageStream**: recover ability of pushing docker image

### Refactor

-   build OCI image with nix2container
-   **terraform**: use OpenTofu instead

## v0.18.0 (2023-11-27)

### BREAKING CHANGE

-   Our devshell module is located now at `precommix.devshellModules.default` and is
    multi-arch.
-   You have to add the overlay `precommix.overlays.default` on the `pkgs` used for
    building your dev shell.
-   poetry support removed.

### Feat

-   support overlaid precommix-env

### Fix

-   **oca-gen-addon-readme**: fix build and test it
-   avoid using builtins.storePath in pure evaluation mode
-   allow solving precommix-env in pure evaluation mode
-   **ctt**: undo update to v2
-   **ctt**: check untracked files directly on hook
-   typo on exclusion regexp
-   avoid checking some auto-generated lock files
-   flake-parts compatibility out of the box

### Refactor

-   remove poetry2nix and npmlock2nix and use dream2nix
-   drop support for poetry checks

## v0.17.1 (2023-09-05)

### Fix

-   support nix 2.17 again on CI
-   remove unsafe features
-   pin nix 2.15 docker image on gitlab ci

## v0.17.0 (2023-05-22)

### Feat

-   **taplo**: new hook for formatting TOML files
-   **nix**: include numtide binary cache
-   allow updating .nix files
-   **flake**: include default nix config
-   move imageStream output from packages to apps
-   recommend vscode integration extension
-   **js**: add eslint configuration file, with good defaults for Odoo
-   **ctt**: include defaults section

### Fix

-   **ctt**: run hook always
-   **ctt**: don't change answers file whitespace
-   **ctt**: reproducible `_src_path`
-   **pre-commit**: force hook install
-   **oca-maintainers-tools**: include readme jinja template
-   **ctt**: remove hack for solved issue

## v0.16.1 (2023-04-21)

### Fix

-   **autoflake**: make it work again

## v0.16.0 (2023-04-21)

### Feat

-   **nothing-added**: make the new hook a script of its own
-   **nothing-added**: new pre-commit hook

### Fix

-   **ctt**: ignore only the answers files
-   **ctt**: patch the patch
-   **ctt**: include needed fix
-   **direnv**: do not use `eval` as an executable

### Refactor

-   update all lock files

## v0.15.0 (2023-04-05)

### Feat

-   gitignore dist folder, useful for Python projects
-   include gitlab-ci definitions

## v0.14.0 (2023-03-29)

### Feat

-   **direnv**: allow parametrizing flake usage
-   **direnv**: use impure flakes by default

### Fix

-   update overlay definition from template flake

## v0.13.0 (2023-02-14)

### Feat

-   **terraform**: add indent-size 2 to `*.tftpl` files

### Fix

-   **poetry-lock**: avoid updating on pre-commit

## v0.12.0 (2023-01-18)

### BREAKING CHANGE

-   Stop providing Copier package, now that upstream supports nix directly.

### Feat

-   add a default .gitignore

### Fix

-   **cz**: default to major_version_zero = true
-   use upstream copier
-   ignore .devenv and .direnv
-   provide `$SSL_CERT_FILE`

## v0.11.0 (2022-12-12)

### Feat

-   **poetry-lock**: enable check by default
-   **buildDevshellModule**: import extra locale package in custom-built devshell
    modules too
-   **devshell**: import extra locale package

### Fix

-   update workarounds and lock file
-   **poetry-lock**: run only when `poetry.lock` file is found

## v0.10.0 (2022-11-28)

### Feat

-   **copier-template-tester**: new hook

## v0.9.0 (2022-11-17)

### Feat

-   **flake**: multiple systems support

## v0.8.1 (2022-11-08)

### Fix

-   assert that a recently-copied template violates no linters, fix failing linter
    (check-toml), update dependencies

## v0.8.0 (2022-11-07)

### Feat

-   **check-toml**: new common check

### Fix

-   include main ref when generating precommix.nix

## v0.7.0 (2022-10-06)

### Feat

-   po linting on all projects

### Fix

-   avoid reformatting /static/src/lib in odoo mode

## v0.6.0 (2022-08-29)

### Feat

-   custom devshell module builder
-   add `lib.${system}.proxyCommand` to flake outputs and advertise terraform
-   provide a devshell module

### Fix

-   avoid nasty extra whitespace that is always added in answers file
-   **gitlab-ci**: remove implicit includes

## v0.5.1 (2022-07-29)

### Fix

-   rename namespace

## v0.5.0 (2022-07-29)

### Feat

-   expose predictable package names

## v0.4.2 (2022-07-29)

### Fix

-   default to nix mode in gitlab ci
-   remove separate pre-commit package
-   project moved from namespace
-   avoid warning when downloading precommix

## v0.4.1 (2022-07-26)

### Fix

-   self-update

## v0.4.0 (2022-07-26)

### Fix

-   warn .gitlab-ci.yml is only for internal usage
-   **terraform-fmt**: make hook work out of the box
-   **template**: remove unexisting precommix input

### BREAKING CHANGE

-   Gitlab CI option only works for Moduon repos now.

## v0.3.2 (2022-06-13)

### Fix

-   do not skip precommix.nix

## v0.3.1 (2022-06-13)

### Fix

-   restore support for `name-tests-test --pytest-test-first`

## v0.3.0 (2022-06-13)

### Fix

-   use builtins.fetchgit for pinning precommix
-   remove shared input optimizations
-   **copier**: inverted choice answers
-   simpler terraform file detection
-   **terraform**: properly select files to check
-   **terraform**: terraform-fmt hook serial
-   clarify what "supporting" means
-   clarify who are "we"

### Feat

-   pin precommix version in `flake.lock`
-   expose individual packages from precommix-env
-   let pydocstyle read pyproject.toml for configuration
-   supply default .gitlab-ci.yml file
-   **terraform**: support some terraform hooks

## v0.2.0 (2022-05-27)

### Feat

-   **copier**: supply temporarily the copier package

## v0.1.0 (2022-05-26)

### Feat

-   add commitizen configuration
-   remove `coding: utf-8` comment by default
-   prefer LF over CRLF line endings by default
-   remove hook, deprecated upstream
-   conditional hooks
-   **docker**: include poetry and pre-commit
-   **template**: add standard config files
-   exist

### Fix

-   remove language version
-   use hooks definition to know required binaries
-   link only binaries, and only some of them
-   **docker**: more tools needed for the image
-   **docker**: add required packages to execute CI jobs
-   **pre-commit**: avoid `poetry run` for local hooks
-   **template**: track answers
-   **template**: make post-tasks work, and continue on failure
-   **template**: include nixpkgs properly, remove @inputs
-   **template**: workaround gitlab scheme problem with subgroups
-   allow template to validate

### Perf

-   **template**: reuse inputs
