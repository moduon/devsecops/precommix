# Here we only supply what goes into flake's `checks` output
{self, ...}: {
  perSystem = {
    pkgs,
    self',
    ...
  }: let
    commonInputs = {
      GIT_AUTHOR_EMAIL = "test@example.com";
      GIT_AUTHOR_NAME = "test";
      GIT_COMMITTER_EMAIL = "test@example.com";
      GIT_COMMITTER_NAME = "test";
      shell = self'.devShells.ci;
      src = ./.;
    };
  in {
    checks = {
      testCopyTemplate =
        pkgs.runCommandLocal
        "testCopyTemplate"
        (commonInputs
          // {
            copier = self'.legacyPackages.precommix-deps.config.pip.drvs.copier.public;
          })
        ''
          export PATH=$shell/bin:$PATH
          export HOME=$NIX_BUILD_TOP/home
          mkdir $HOME
          touch $out

          # Init repo for src
          export GIT_DIR=$NIX_BUILD_TOP/src-git GIT_WORK_TREE=$src
          git init
          git add $src
          git commit -m hello
          unset -v GIT_DIR GIT_WORK_TREE

          # Copy template
          $copier/bin/copier copy -f $src dest
          chmod -R u+w dest

          # Modify
          cd dest
          git init
          git add -A
          sed -i s%https://gitlab.com/moduon/precommix%$NIX_BUILD_TOP/src-git% .pre-commit-config.yaml
          pre-commit run -a --show-diff-on-failure || (cat $HOME/.cache/pre-commit/pre-commit.log && false)
        '';

      # Tests inspired by https://github.com/NixOS/nixpkgs/blob/3452f773bbc5a143439cd5229655b4329235df08/pkgs/applications/networking/cluster/opentofu/default.nix#L71-L94
      testOverriddenTofu = let
        testInputs = {
          mainTf = ''
            terraform {
              required_providers {
                random = {
                  source  = "registry.terraform.io/hashicorp/random"
                }
              }
            }

            resource "random_id" "test" {}
          '';
          shell = pkgs.devshell.mkShell {
            imports = [
              self.devshellModules.default
            ];
            commands = [
              {package = pkgs.lib.hiPrio (pkgs.opentofu.withPlugins (ps: [ps.random]));}
            ];
          };
          # Make it fail outside of sandbox
          HTTP_PROXY = "http://127.0.0.1:0";
          HTTPS_PROXY = "https://127.0.0.1:0";
        };
      in
        pkgs.runCommandLocal
        "testOverriddenDevshellModule"
        testInputs
        ''
          mkdir $out
          echo "$mainTf" > $out/main.tf
          $shell/bin/tofu -chdir=$out init
          ls -AR $out

              # Assertions
              test -d $out/.terraform
              test -f $out/.terraform.lock.hcl
              test -f $out/main.tf
        '';

      testPylintOdooLoaded = pkgs.runCommandLocal "testPylintOdooLoaded" commonInputs ''
        $shell/bin/pylint --load-plugins pylint_odoo --list-groups | grep odoolint
        touch $out
      '';

      # Check we're able to import the project without flakes and still be able to
      # build it in pure mode; otherwise you can hit
      # https://github.com/nix-community/home-manager/issues/2409 or similar issues
      testPureSelfBuild = (import ./.).packages.${pkgs.system}.default;
    };
  };
}
