{
  config,
  dream2nix,
  ...
}: let
  prettierVersion = config.nodejs-package-lock-v3.packageLock.packages."node_modules/prettier".version;
  prettierXMLPluginVersion = config.nodejs-package-lock-v3.packageLock.packages."node_modules/@prettier/plugin-xml".version;
in {
  imports = [
    dream2nix.modules.dream2nix.nodejs-granular-v3
    dream2nix.modules.dream2nix.nodejs-package-json-v3
  ];

  name = "prettier-env";
  version = "${prettierVersion}+${prettierXMLPluginVersion}";

  mkDerivation = {
    src = ./.;
    fixupPhase = ''
      wrapProgram $out/bin/prettier --add-flags "--plugin $out/lib/node_modules/$pname/node_modules/@prettier/plugin-xml/src/plugin.js"
    '';
    doInstallCheck = true;
    installCheckPhase = ''
      # Create a test file that prettier-xml won't like
      cat <<END > test.xml
      <?xml version="1.0" encoding="UTF-8"?>
      <root  fixme="please"  />
      END

      # Fix it
      $out/bin/prettier --write test.xml || true

      # Check that it was fixed
      $out/bin/prettier --check test.xml
    '';
  };
}
