{
  config,
  dream2nix,
  ...
}: {
  imports = [
    dream2nix.modules.dream2nix.nodejs-granular-v3
    dream2nix.modules.dream2nix.nodejs-package-json-v3
  ];

  name = "eslint-env";
  inherit (config.nodejs-package-lock-v3.packageLock.packages."node_modules/eslint") version;

  mkDerivation = {
    src = ./.;
    fixupPhase = ''
      wrapProgram $out/bin/eslint --prefix NODE_PATH : $out/lib/node_modules/$pname/node_modules
    '';

    doInstallCheck = true;
    installCheckPhase = ''
      # Common eslint configuration
      cat <<END > .eslintrc.json
      { "extends": ["eslint:recommended", "prettier"] }
      END

      # This JS code would fail with `eslint:recommended` but pass with `prettier`
      # DOCS https://eslint.org/docs/latest/rules/no-extra-semi
      cat <<END > test.js
      true;;
      END

      $out/bin/eslint test.js
    '';
  };
}
