"""Post-build hook for precommix-env derivation."""

import os
import sys
from pathlib import Path

import yaml

# Get all pre-commit hooks and the executables associated with them
HOOKS = yaml.safe_load(sys.argv[1])
hook_ids = set()
wanted = set()

for hook in HOOKS:
    assert hook["id"] not in hook_ids
    assert (
        "language_version" not in hook
    ), f"Unsupported `language_version` in hook {hook['id']}"
    hook_ids.add(hook["id"])
    if hook["language"] != "system":
        assert (
            hook["language"] == "fail"
        ), f"Unsupported hook language {hook['language']}"
        continue
    executable = hook["entry"].split()[0]
    print(f"keeping {executable}")
    wanted.add(executable)

wanted_wrapped = {f".{executable}-wrapped" for executable in wanted}

# Remove all unused binaries
for executable in Path(os.environ["out"], "bin").iterdir():
    if executable.name not in wanted | wanted_wrapped:
        print(f"removing {executable}")
        executable.unlink()
    wanted.discard(executable.name)

# Make sure all wanted executables were found
assert not wanted, f"Wanted executables not found: {wanted}"
