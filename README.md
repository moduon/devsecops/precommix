# Precommix

A pre-commit mix with nix.

## Why

[Pre-commit](https://pre-commit.com/) is excellent at managing git workflow hooks.

However it includes a part of package management. It supports many package managers, and
inherits all weaknesses from all of them.

[Nix](https://nixos.org/) is the most awesome package manager ever made, but doesn't
handle pre-commit stages.

This project installs all our required pre-commit hooks using nix and then makes use of
them with pre-commit.

## Quick start

Are you using a project that already uses Precommix and you just want to hack on it
ASAP?

1. [Install nix](https://nixos.org/download.html), if you haven't already:

    ```bash
    sh <(curl -L https://nixos.org/nix/install) --daemon
    ```

2. Enter the development shell:

    ```bash
    nix --extra-experimental-features 'nix-command flakes' develop --impure --accept-flake-config
    ```

Ready! 😎

```bash
pre-commit run -a
```

### Better integration

You want more automation, right? Enter [Direnv](https://direnv.net/), a tool that will
hook into your shell and automatically load prebuilt environments whenever you `cd` into
a folder.

1. [Install direnv](https://direnv.net/docs/installation.html).
2. [Configure direnv to hook into your shell](https://direnv.net/docs/hook.html).
3. Allow the project's configuration:

    ```bash
    cd path/to/project
    direnv allow
    ```

Done! 🚀

Now, each time you `cd` into the project's folder, instead of having to enter the
development shell, it will be automatically loaded into your current shell.

You can also install the recommended VSCode extensions to integrate Direnv with VSCode.

## Enabling Precommix into a project

Just apply the [Copier](https://copier.readthedocs.io/) template and commit the changes:

```bash
copier copy https://gitlab.com/moduon/precommix .

# Do this 1st time too
git init
git add -A
direnv allow || true
```

Behind the scenes, the template is not much more than:

-   A [pre-commit config file](https://pre-commit.com/#plugins) where
    [all hooks use `language: system`](https://github.com/numtide/devshell/issues/16#issuecomment-1124832534).
-   A [nix flake](https://nixos.wiki/wiki/Flakes) that configures a
    [devshell](https://github.com/numtide/devshell) where `precommix-env` and
    `pre-commit` packages are
    [installed using nix](https://nixos.wiki/wiki/Development_environment_with_nix-shell).
-   A [compatibility layer with stable nix](https://github.com/edolstra/flake-compat).

### If you were already using nix

In that case, the `default.nix`, `flake.nix` and `shell.nix` files supplied by the
template will probably not be good enough for you.

Most of details are very important. Just take a look at those files in the template and
get inspired from them.

However, follow this tip: import precommix from `precommix.nix`, like this:

```nix
{
    outputs =
        let precommix = import ./.precommix.nix;
        in {...};
}
```

This way, you will pin the same precommix version in `.copier-answers.yml`,
`.pre-commit-config.yaml` and `flake.lock` files.

### Use the devshell module

If you're already a user of [devshell](https://github.com/numtide/devshell), use the
provided module in your `flake.nix` file like this:

```nix
{
    inputs.devshell.url = github:numtide/devshell;
    inputs.nixpkgs.url = <nixpkgs>;
    outputs = inputs:
        let
            system = "x86_64-linux";
            pkgs = import inputs.nixpkgs {
                inherit system;
                overlays = [inputs.devshell.overlay];
            };
            precommix = import ./precommix.nix;
        in {
            devShells.${system}.default = pkgs.devshell.mkShell {
                imports = [precommix.devshellModules.${system}.default];
            };
        };
}
```

That should make pre-commit hooks auto-installed in your devshell when you enter it, and
all the required packages will be available out of the box.

If some package provided by the default `precommix-env` package conflicts with the local
package you need, you can override it using the provided
`lib.${system}.buildDevshellModule`:

```nix
{
    inputs.devshell.url = github:numtide/devshell;
    inputs.nixpkgs.url = <nixpkgs>;
    outputs = inputs:
        let
            system = "x86_64-linux";
            pkgs = import inputs.nixpkgs {
                inherit system;
                overlays = [inputs.devshell.overlay];
            };
            precommix = import ./precommix.nix;

            # Example: opentofu with plugins
            opentofu = pkgs.opentofu.withPlugins (ps: [ps.hcloud]);
        in {
            devShells.${system}.default = pkgs.devshell.mkShell {
                imports = [
                    # Load overridden module
                    (precommix.lib.${system}.buildDevshellModule {
                        extraPackages = {inherit opentofu;};
                    })
                ];
            };
        };
}
```

### Use the flake-parts module

[Flake Parts](https://flake.parts/) makes Nix flake composition easier.

A minimal flake for precommix would look like this:

```nix
{
  outputs = inputs @ {flake-parts, nixpkgs}:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [(import ./precommix.nix).flakeModules.devshell];
    };
}
```

### Docker integration

While applying the template, you can tell copier to enable built-in Gitlab CI
configuration to run the hooks inside Nix's official docker image.

We also provide tagged docker images. You can use those directly if you only use tagged
releases.

Alternatively, you can just built your own image:

```bash
nix run .#imageStream | docker load
```

## Who

Created and maintained by [Moduon Team](https://www.moduon.team/).

Original idea by [Jairo Llopis](https://www.recallstack.icu/).

## Where

Anywhere you want! 🎁 It's [GPL 3.0+](./LICENSE).
